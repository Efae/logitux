# logitux

Logitech device's management app

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Before running
execute the following commands to allow the app to access the usb devices
```bash
sudo chmod -R  777 /dev/bus/usb/
sudo modprobe usbmon
sudo setfacl -m u:${USER}:r /dev/usbmon*
```
or create udev rules
