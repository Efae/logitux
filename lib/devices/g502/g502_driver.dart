import 'package:libusb_new/libusb_new.dart';

import '../usb_device.dart';

class G502Driver extends UsbDevice {
  G502Driver()
      : super(vendorId: 0x046d, productId: 0xc08b, displayName: 'Mouse G502 Hero');

  setPPP(int ppp) {
    sendDataOnce(
        1,
        0x21,
        libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION,
        0x0210,
        0x82,
        '10ff0a3a00${ppp.toRadixString(16).padLeft(4, '0')}');
  }
}
