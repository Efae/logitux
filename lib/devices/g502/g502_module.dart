import 'package:flutter_modular/flutter_modular.dart';
import 'package:logitux/devices/g502/g502_driver.dart';
import 'package:logitux/devices/g502/g502_page.dart';
import 'package:logitux/devices/g502/screen/ppp_screen.dart';

class G502Module extends Module {
  final G502Driver driver;

  G502Module({required device}) : driver = device.driver as G502Driver;

  @override
  void routes(r) {
    r.child('/', child: (context) => G502Page(driver: driver), children: [
        ChildRoute('/ppp',
        child: (context) => PPPScreen(driver: driver),
        transition: TransitionType.noTransition)
    ]);
  }
}
