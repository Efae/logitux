import 'dart:convert';
import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';

import 'package:ffi/ffi.dart';
import 'package:libusb_new/libusb_new.dart';
import 'package:logitux/device_model.dart';

abstract class UsbDevice {
  final int _vendorId;
  final int _productId;
  final String _displayName;

  late final _libusb = Libusb(_loadLibrary());

  UsbDevice({required int vendorId, required int productId, required String displayName})
  : _vendorId = vendorId, _productId = productId, _displayName = displayName;

  int get vendorId => _vendorId;
  int get productId => _productId;
  String get name => _displayName;

  static DynamicLibrary _loadLibrary() {
    if (Platform.isLinux) {
      return DynamicLibrary.open(
          '${Directory.current.path}/resources/libusb-1.0/libusb-1.0.so');
    }
    throw UnsupportedError('Unsupported platform');
  }

  Pointer<libusb_device_handle> openConnection(int interface) {
    var init = _libusb.libusb_init(nullptr);
    if (init != libusb_error.LIBUSB_SUCCESS) {
      throw StateError('init error: ${_libusb.describeError(init)}');
    }

    Pointer<libusb_device_handle> devHandle =
        _libusb.libusb_open_device_with_vid_pid(nullptr, _vendorId, _productId);
    _libusb.libusb_set_auto_detach_kernel_driver(devHandle, 1);
    _libusb.libusb_claim_interface(devHandle, interface);
    return devHandle;
  }

  void closeConnection(Pointer<libusb_device_handle> devHandle, int interface) {
    _libusb.libusb_release_interface(devHandle, interface);
    _libusb.libusb_reset_device(devHandle);
    _libusb.libusb_exit(nullptr);
  }

  void sendData(
      Pointer<libusb_device_handle> devHandle,
      int interface,
      int requestType,
      int bRequest,
      int wValue,
      int endpointAddress,
      String data) {
    BigInt bigIntData = BigInt.parse(data, radix: 16);
    Uint8List byteArray = _bigIntToUint8List(bigIntData);
    var dataPtr = calloc<UnsignedChar>(byteArray.length);

    for (int i = 0; i < byteArray.length; i++) {
      dataPtr[i] = byteArray[i];
    }

    _libusb.libusb_control_transfer(devHandle, requestType, bRequest, wValue,
        interface, dataPtr, byteArray.length, 500);
    var dataOutPtr = calloc<UnsignedChar>(20);
    var actualLength = calloc<Int>();
    // listen back from device to check if the data is sent successfully
    _libusb.libusb_interrupt_transfer(
        devHandle, endpointAddress, dataOutPtr, 20, actualLength, 1000);
    print(
        'Data out: ${dataOutPtr.cast<Uint8>().asTypedList(actualLength.value)}');

    calloc.free(dataPtr);
    calloc.free(dataOutPtr);
    calloc.free(actualLength);
  }

  void sendDataOnce(int interface, int requestType, int bRequest, int wValue,
      int endpointAddress, String data) {
    Pointer<libusb_device_handle> devHandle = openConnection(interface);

    sendData(devHandle, interface, requestType, bRequest, wValue,
        endpointAddress, data);

    closeConnection(devHandle, interface);
  }

  Uint8List _bigIntToUint8List(BigInt bigInt) =>
      _bigIntToByteData(bigInt).buffer.asUint8List();

  ByteData _bigIntToByteData(BigInt bigInt) {
    final data = ByteData((bigInt.bitLength / 8).ceil());
    var _bigInt = bigInt;

    for (var i = 1; i <= data.lengthInBytes; i++) {
      data.setUint8(data.lengthInBytes - i, _bigInt.toUnsigned(8).toInt());
      _bigInt = _bigInt >> 8;
    }

    return data;
  }

  static List<DeviceModel> getDeviceList() {
    final libusb = Libusb(_loadLibrary());
    var init = libusb.libusb_init(nullptr);
    if (init != libusb_error.LIBUSB_SUCCESS) {
      throw StateError('init error: ${libusb.describeError(init)}');
    }

    var deviceListPtr = calloc<Pointer<Pointer<libusb_device>>>();
    try {
      var count = libusb.libusb_get_device_list(nullptr, deviceListPtr);
      print('Devices found: $count');
      if (count < 0) {
        return [];
      }
      try {
        return List.from(_iterableDeviceInfo(libusb, deviceListPtr.value));
      } finally {
        libusb.libusb_free_device_list(deviceListPtr.value, 1);
      }
    } finally {
      calloc.free(deviceListPtr);
      libusb.libusb_exit(nullptr);
    }
  }

  static Iterable<DeviceModel> _iterableDeviceInfo(
      Libusb libusb,
      Pointer<Pointer<libusb_device>> deviceList) sync* {
    var descPtr = calloc<libusb_device_descriptor>();
    var devHandlePtr = calloc<Pointer<libusb_device_handle>>();
    final strDescLength = 254;
    var strDescPtr = calloc<UnsignedChar>(strDescLength);

    for (var i = 0; deviceList[i] != nullptr; i++) {
      var devDesc =
          libusb.libusb_get_device_descriptor(deviceList[i], descPtr);
      if (devDesc != libusb_error.LIBUSB_SUCCESS) {
        print('devDesc error: ${libusb.describeError(devDesc)}');
        continue;
      }
      var idVendor = descPtr.ref.idVendor;
      var idProduct = descPtr.ref.idProduct;
      var idVendorString = idVendor.toRadixString(16).padLeft(4, '0');
      var idProductString = idProduct.toRadixString(16).padLeft(4, '0');

      if (descPtr.ref.iProduct == 0) {
        print('$idVendorString:$idProductString iProduct empty');
        yield DeviceModel(idVendor, idProduct, '');
        continue;
      }

      var open = libusb.libusb_open(deviceList[i], devHandlePtr);
      if (open != libusb_error.LIBUSB_SUCCESS) {
        print(
            '$idVendorString:$idProductString open error: ${libusb.describeError(open)}');
        yield DeviceModel(idVendor, idProduct, '');
        continue;
      }
      var devHandle = devHandlePtr.value;

      try {
        var langDesc = libusb.inline_libusb_get_string_descriptor(
            devHandle, 0, 0, strDescPtr, strDescLength);
        if (langDesc < 0) {
          print(
              '$idVendorString:$idProductString langDesc error: ${libusb.describeError(langDesc)}');
          yield DeviceModel(idVendor, idProduct, '');
          continue;
        }
        var langId = strDescPtr[2] << 8 | strDescPtr[3];

        var prodDesc = libusb.inline_libusb_get_string_descriptor(
            devHandle, descPtr.ref.iProduct, langId, strDescPtr, strDescLength);
        if (prodDesc < 0) {
          print(
              '$idVendorString:$idProductString prodDesc error: ${libusb.describeError(prodDesc)}');
          yield DeviceModel(idVendor, idProduct, '');
          continue;
        }

        yield DeviceModel(idVendor, idProduct,
            utf8.decode(strDescPtr.cast<Uint8>().asTypedList(prodDesc)));
        continue;
      } finally {
        libusb.libusb_close(devHandle);
      }
    }

    calloc.free(descPtr);
    calloc.free(devHandlePtr);
    calloc.free(strDescPtr);
  }
}

const int _kMaxSmi64 = (1 << 62) - 1;
const int _kMaxSmi32 = (1 << 30) - 1;
final int _maxSize = sizeOf<IntPtr>() == 8 ? _kMaxSmi64 : _kMaxSmi32;

extension LibusbExtension on Libusb {
  String describeError(int error) {
    var array = libusb_error_name(error);
    // FIXME array is Pointer<Char>, not Pointer<Uint8>
    var nativeString = array.cast<Uint8>().asTypedList(_maxSize);
    var strlen = nativeString.indexWhere((char) => char == 0);
    return utf8.decode(array.cast<Uint8>().asTypedList(strlen));
  }
}

extension LibusbInline on Libusb {
  /// [libusb_get_string_descriptor]
  int inline_libusb_get_string_descriptor(
    Pointer<libusb_device_handle> dev_handle,
    int desc_index,
    int langid,
    Pointer<UnsignedChar> data,
    int length,
  ) {
    return libusb_control_transfer(
      dev_handle,
      libusb_endpoint_direction.LIBUSB_ENDPOINT_IN,
      libusb_standard_request.LIBUSB_REQUEST_GET_DESCRIPTOR,
      libusb_descriptor_type.LIBUSB_DT_STRING << 8 | desc_index,
      langid,
      data,
      length,
      1000,
    );
  }
}
