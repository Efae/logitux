import 'package:libusb_new/libusb_new.dart';

import '../usb_device.dart';

class BlueYetiDriver extends UsbDevice {
  BlueYetiDriver()
      : super(vendorId: 0x046d, productId: 0x0ab7, displayName: 'Blue Yeti');
}
