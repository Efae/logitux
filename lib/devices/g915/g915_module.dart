import 'package:flutter_modular/flutter_modular.dart';
import 'package:logitux/devices/g915/g915_driver.dart';

class G915Module extends Module {
  final G915Driver driver;

  G915Module({required device}) : driver = device.driver as G915Driver;

  @override
  void routes(r) {
    // TODO: implement routes
  }
}
