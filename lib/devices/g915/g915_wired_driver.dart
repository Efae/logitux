import 'dart:ffi';

import 'package:libusb_new/libusb_new.dart';
import 'package:logitux/devices/g915/g915_driver.dart';

class G915WiredDriver extends G915Driver {
  G915WiredDriver() : super(productId: 0xc33e);

  void _initKeyboard(Pointer<libusb_device_handle> device) {
    sendData(
        device,
        0x0002,
        0x21,
        libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION,
        0x0211,
        0x83,
        '11ff091a00000000000000000000000001000000');
    sendData(
        device,
        0x0002,
        0x21,
        libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION,
        0x0211,
        0x83,
        '11ff091a01000000000000000000000001000000');
    sendData(
        device,
        0x0002,
        0x21,
        libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION,
        0x0211,
        0x83,
        '11ff0a5a2f6200ffff012d00ffff686f00ffff00');
    sendData(
        device,
        0x0002,
        0x21,
        libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION,
        0x0211,
        0x83,
        '11ff0a5a6367000000b4b800ffff9b9e00ffff00');
    sendData(
        device,
        0x0002,
        0x21,
        libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION,
        0x0211,
        0x83,
        '11ff0a5a2e2e000000999900ffffd2d200ffff00');
  }

  void confirm(Pointer<libusb_device_handle> device) {
    sendData(
        device,
        0x0002,
        0x21,
        libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION,
        0x0211,
        0x83,
        '11ff0a7a00000000000000000000000000000000');
  }

  void setColor(String color) {
    Pointer<libusb_device_handle> device = openConnection(0x0002);
    _initKeyboard(device);
    sendData(
        device,
        0x0002,
        0x21,
        libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION,
        0x0211,
        0x83,
        '11ff0a5a9b9e${color}00000000000000000000');
    confirm(device);
    closeConnection(device, 0x0002);
  }
}
