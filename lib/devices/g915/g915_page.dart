import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:logitux/device_model.dart';
import 'package:logitux/devices/g915/g915_driver.dart';

class G915Page extends StatefulWidget {
  final DeviceModel device;

  const G915Page({super.key, required this.device});

  @override
  State<G915Page> createState() => _G915PageState();
}

class _G915PageState extends State<G915Page> {
  Color currentColor = Colors.white;
  Color pickerColor = Colors.white;

  void changeColor(Color color) {
    setState(() => pickerColor = color);
  }

  @override
  Widget build(BuildContext context) {
    final driver = widget.device.driver as G915Driver;
    return Scaffold(
        appBar: AppBar(
          title: Text(driver.name),
        ),
        body: Row(
          children: [
            Expanded(child: Container(color: Colors.deepPurple)),
            Expanded(
                flex: 4,
                child: Container(
                  color: Colors.deepPurple[100],
                  child: Center(
                      child: Row(children: [
                        ConstrainedBox(
                            constraints: const BoxConstraints(maxWidth: 300),
                            child: Padding(
                                padding: const EdgeInsets.all(6),
                                child: Card(
                                    elevation: 2,
                                    child: ColorPicker(
                                      pickersEnabled: const <ColorPickerType, bool>{
                                        ColorPickerType.wheel: true
                                      },
                                      onColorChanged: (color) {
                                        setState(() => pickerColor = color);
                                        // print(color.toHexString());
                                        // driver.setColor(color.toHexString());
                                      },
                                    )))),
                        ElevatedButton(
                            onPressed: () {
                              print(
                                  pickerColor.value.toRadixString(16).substring(2));
                              driver.setColor(
                                  pickerColor.value.toRadixString(16).substring(2));
                            },
                            child: const Text('Set Color'))
                      ])),
                ))
          ],
        ));
  }
}
