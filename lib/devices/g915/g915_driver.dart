import '../usb_device.dart';

abstract class G915Driver extends UsbDevice {
  G915Driver({required super.productId})
      : super(
      vendorId: 0x046d, displayName: 'Keyboard G915');

  void setColor(String color);
}
