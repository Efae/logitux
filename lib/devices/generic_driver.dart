import 'package:logitux/devices/usb_device.dart';

class GenericDriver extends UsbDevice {
  GenericDriver({required super.productId, super.vendorId = 0x046d, super.displayName = 'Generic'});
}
