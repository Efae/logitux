import 'package:flutter_modular/flutter_modular.dart';
import 'package:logitux/devices/g213/g213.dart';

class G213Module extends Module {
  final G213Driver driver;

  G213Module({required device}) : driver = device.driver as G213Driver;

  @override
  void routes(r) {
    r.child('/', child: (context) => G213Page(driver: driver), children: [
      ChildRoute('/color',
          child: (context) => ColorScreen(driver: driver),
          transition: TransitionType.noTransition),
      ChildRoute('/breath',
          child: (context) => BreathScreen(driver: driver),
          transition: TransitionType.noTransition),
      ChildRoute('/cycle',
          child: (context) => CycleScreen(driver: driver),
          transition: TransitionType.noTransition),
      ChildRoute('/zone',
          child: (context) => ZoneScreen(driver: driver),
          transition: TransitionType.noTransition),
      ChildRoute('/wave',
          child: (context) => WaveScreen(driver: driver),
          transition: TransitionType.noTransition),
      ChildRoute('/disable',
          child: (context) => DisableScreen(driver: driver),
          transition: TransitionType.noTransition)
    ]);
  }
}
