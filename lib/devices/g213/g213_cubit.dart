import 'package:bloc/bloc.dart';

part 'g213_state.dart';

class G213Cubit extends Cubit<G213State> {
  G213Cubit()
      : super(G213State(
            speed: 3000,
            color: 'ffffff',
            zones: ['ffffff', 'ffffff', 'ffffff', 'ffffff', 'ffffff']));

  void setSpeed(double speed) {
    emit(G213State(speed: speed, color: state.color, zones: state.zones));
  }

  void setColor(String color) {
    emit(G213State(speed: state.speed, color: color, zones: state.zones));
  }

  void setZoneColor(String color, int zone) {
    List<String> zones = state.zones;
    zones[zone] = color;
    emit(G213State(speed: state.speed, color: state.color, zones: zones));
  }
}
