import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logitux/devices/g213/g213_cubit.dart';
import 'package:logitux/devices/g213/g213_driver.dart';

class ZoneScreen extends StatelessWidget {
  final G213Driver driver;

  const ZoneScreen({super.key, required this.driver});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Wrap(children: [
        _setZoneColor(1, context),
        _setZoneColor(2, context),
        _setZoneColor(3, context),
        _setZoneColor(4, context),
        _setZoneColor(5, context),
      ])
    );
  }

  Widget _setZoneColor(int zone, BuildContext context) {
    Future<bool> colorPickerDialog() async {
      return ColorPicker(
        // Use the dialogPickerColor as start and active color.
        color: Color(int.parse(
            '0xff${BlocProvider.of<G213Cubit>(context).state.zones[zone - 1]}')),
        // Update the dialogPickerColor using the callback.
        onColorChanged: (Color color) => BlocProvider.of<G213Cubit>(context)
            .setZoneColor(color.value.toRadixString(16).substring(2), zone - 1),
        width: 40,
        height: 40,
        borderRadius: 4,
        spacing: 5,
        runSpacing: 5,
        wheelDiameter: 155,
        heading: Text(
          'Select color',
          style: Theme.of(context).textTheme.titleSmall,
        ),
        subheading: Text(
          'Select color shade',
          style: Theme.of(context).textTheme.titleSmall,
        ),
        wheelSubheading: Text(
          'Selected color and its shades',
          style: Theme.of(context).textTheme.titleSmall,
        ),
        showMaterialName: true,
        showColorName: true,
        showColorCode: true,
        copyPasteBehavior: const ColorPickerCopyPasteBehavior(
          longPressMenu: true,
        ),
        materialNameTextStyle: Theme.of(context).textTheme.bodySmall,
        colorNameTextStyle: Theme.of(context).textTheme.bodySmall,
        colorCodeTextStyle: Theme.of(context).textTheme.bodySmall,
        pickersEnabled: const <ColorPickerType, bool>{
          ColorPickerType.both: false,
          ColorPickerType.primary: true,
          ColorPickerType.accent: true,
          ColorPickerType.bw: false,
          ColorPickerType.custom: true,
          ColorPickerType.wheel: true,
        },
        // customColorSwatchesAndNames: colorsNameMap,
      ).showPickerDialog(
        context,
        // New in version 3.0.0 custom transitions support.
        transitionBuilder: (BuildContext context, Animation<double> a1,
            Animation<double> a2, Widget widget) {
          final double curvedValue =
              Curves.easeInOutBack.transform(a1.value) - 1.0;
          return Transform(
            transform: Matrix4.translationValues(0.0, curvedValue * 200, 0.0),
            child: Opacity(
              opacity: a1.value,
              child: widget,
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 400),
        constraints:
            const BoxConstraints(minHeight: 460, minWidth: 300, maxWidth: 320),
      );
    }

    return ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 200),
        child: Card(
            child: ListTile(
          title: Text('Zone $zone'),
          trailing: BlocBuilder<G213Cubit, G213State>(
              builder: (context, state) => ColorIndicator(
                    width: 44,
                    height: 44,
                    borderRadius: 22,
                    color: Color(int.parse('0xff${state.zones[zone - 1]}')),
                    onSelect: () async {
                      // Store current color before we open the dialog.
                      final String colorBeforeDialog = BlocProvider.of<G213Cubit>(context).state.zones[zone - 1];
                      // Wait for the picker to close, if dialog was dismissed,
                      // then restore the color we had before it was opened.
                      if (!(await colorPickerDialog())) {
                        BlocProvider.of<G213Cubit>(context).setZoneColor(colorBeforeDialog, zone - 1);
                      } else {
                        G213State state =
                            BlocProvider.of<G213Cubit>(context).state;
                        driver.setZone(zone, state.zones[zone - 1]);
                      }
                    },
                  )),
        )));
  }
}
