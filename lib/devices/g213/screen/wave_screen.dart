import 'package:flutter/material.dart';
import 'package:logitux/devices/g213/g213_driver.dart';

class WaveScreen extends StatelessWidget {
  final G213Driver driver;

  const WaveScreen({super.key, required this.driver});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Wrap(
      children: [
        _buildButton(WaveType.horizontal),
        _buildButton(WaveType.decentring),
        _buildButton(WaveType.centering),
        _buildButton(WaveType.flipHorizontal),
      ],
    ));
  }

  Widget _buildButton(WaveType waveType) {
    return Container(
        margin: const EdgeInsets.only(left: 10, right: 10),
        child: ElevatedButton(
            onPressed: () => driver.setWave(waveType),
            child: Text(waveType.displayName)));
  }
}
