import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logitux/devices/g213/g213_cubit.dart';
import 'package:logitux/devices/g213/g213_driver.dart';

class BreathScreen extends StatelessWidget {
  final G213Driver driver;

  const BreathScreen({super.key, required this.driver});

  @override
  Widget build(BuildContext context) {
    return Center(child: Column(mainAxisSize: MainAxisSize.min, children: [
      Container(
          constraints: const BoxConstraints(maxWidth: 300),
          child: Padding(
              padding: const EdgeInsets.all(6),
              child: Card(
                  elevation: 2,
                  child: BlocBuilder<G213Cubit, G213State>(
                      builder: (context, state) => ColorPicker(
                            color: Color(int.parse('0xff${state.color}')),
                            pickersEnabled: const <ColorPickerType, bool>{
                              ColorPickerType.wheel: true,
                              ColorPickerType.accent: false,
                              ColorPickerType.primary: false,
                            },
                            onColorChanged: (color) {
                              BlocProvider.of<G213Cubit>(context).setColor(
                                  color.value.toRadixString(16).substring(2));
                            },
                          ))))),
      BlocBuilder<G213Cubit, G213State>(
          builder: (context, state) => Slider(
                max: 20000,
                min: 1000,
                value: state.speed,
                divisions: 100,
                onChanged: (double value) {
                  BlocProvider.of<G213Cubit>(context).setSpeed(value);
                },
              )),
      ElevatedButton(
          onPressed: () {
            G213State state = BlocProvider.of<G213Cubit>(context).state;
            driver.setBreath(state.color, state.speed.toInt());
          },
          child: const Text('Set Breath'))
    ]));
  }
}
