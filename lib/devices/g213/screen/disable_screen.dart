import 'dart:async';

import 'package:flutter/material.dart';
import 'package:logitux/devices/g213/g213.dart';

class DisableScreen extends StatefulWidget {
  final G213Driver driver;

  const DisableScreen({super.key, required this.driver});

  @override
  State<DisableScreen> createState() => _DisableScreenState();
}

class _DisableScreenState extends State<DisableScreen> {
  late G213Driver driver;
  // StreamController<int>? keyStream;

  @override
  void initState() {
    super.initState();
    driver = widget.driver;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Row(children: [
      ElevatedButton(
          onPressed: () {
            // _startListening();
          },
          child: const Text('Listen')),
      ElevatedButton(
          onPressed: () {
            // keyStream?.close();
            // keyStream = null;
          },
          child: const Text('Stop')),
    ]));
  }

  @override
  void dispose() {
    super.dispose();
    // keyStream = null;
  }

  // void _startListening() {
  //   if (keyStream != null) return;
  //   keyStream ??= driver.listenKeyPress();
  //   keyStream!.stream.listen((event) {
  //     print('Key: ${event.toRadixString(16)}');
  //   });
  // }
}
