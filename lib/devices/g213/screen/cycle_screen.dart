import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logitux/devices/g213/g213_cubit.dart';
import 'package:logitux/devices/g213/g213_driver.dart';

class CycleScreen extends StatelessWidget {
  final G213Driver driver;

  const CycleScreen({super.key, required this.driver});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: BlocBuilder<G213Cubit, G213State>(
            builder: (context, state) => Slider(
                  max: 20000,
                  min: 1000,
                  value: state.speed,
                  divisions: 100,
                  onChanged: (double value) {
                    BlocProvider.of<G213Cubit>(context).setSpeed(value);
                  },
                  onChangeEnd: (double value) {
                    driver.setCycle(value.toInt());
                  },
                )));
  }
}
