import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:logitux/devices/abstract_page.dart';
import 'package:logitux/devices/g213/g213_driver.dart';
import 'package:material_symbols_icons/symbols.dart';

import 'g213_cubit.dart';

class G213Page extends StatelessWidget {
  final G213Driver driver;
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  G213Page({super.key, required this.driver});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (BuildContext context) => G213Cubit(),
        child: AbstractPage(
            title: driver.name,
            baseRoute: '/g213',
            menuItems: {
              Icons.light_mode_outlined:
                const {'Color': '/color', 'Breath': '/breath', 'Cycle': '/cycle', 'Zone': '/zone', 'Wave': '/wave'}
              ,
              Icons.add_box_outlined: const {'Breath': '/breath'},
              Symbols.joystick: const {'Disable': '/disable'}
            })
        );
  }

  Widget _menuCategory(IconData icon) {
    return Container(
        margin: const EdgeInsets.all(10),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
          child: Icon(icon),
          onPressed: () {
            print('Other Mode');
          },
        ));
  }

  Widget _menuTile(String title, String path) {
    return ListTile(
      selected: Modular.to.path.endsWith(path),
      selectedTileColor: Colors.deepPurple[100],
      textColor: Colors.white,
      title: Text(title),
      onTap: () {
        Modular.to.navigate('/g213$path');
      },
    );
  }
}
