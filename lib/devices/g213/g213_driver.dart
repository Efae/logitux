import 'package:libusb_new/libusb_new.dart';

import '../usb_device.dart';

enum WaveType {
  horizontal('Horizontal'),
  decentring('Decentering'),
  centering('Centering'),
  flipHorizontal('Flip Horizontal');

  final String displayName;
  const WaveType(this.displayName);
}

class G213Driver extends UsbDevice {
  static const int requestType = 0x21;
  static const int request =
      libusb_standard_request.LIBUSB_REQUEST_SET_CONFIGURATION;
  static const int value = 0x0211;
  static const int index = 0x82;

  G213Driver()
      : super(
            vendorId: 0x046d, productId: 0xc336, displayName: 'Keyboard G213');

  void setBreath(String color, int speed) {
    _sendData(
        '11ff0c3a0002$color${speed.toRadixString(16).padLeft(4, '0')}006400000000000000');
  }

  void setCycle(int speed) {
    _sendData(
        '11ff0c3a0003ffffff0000${speed.toRadixString(16).padLeft(4, '0')}64000000000000');
  }

  void setColor(String color) {
    _sendData('11ff0c3a0001${color}0200000000000000000000');
  }

  void setZone(int zone, String color) {
    _sendData('11ff0c3a0${zone}01${color}0200000000000000000000');
  }

  void setWave(WaveType wave) {
    switch (wave) {
      case WaveType.horizontal:
        _sendData('11ff0c3c00040000000000008801641300000000');
        break;
      case WaveType.decentring:
        _sendData('11ff0c3c00040000000000008803641300000000');
        break;
      case WaveType.centering:
        _sendData('11ff0c3c00040000000000008808641300000000');
        break;
      case WaveType.flipHorizontal:
        _sendData('11ff0c3c00040000000000008806641300000000');
        break;
      default:
        break;
    }
  }

  void _sendData(String data) {
    sendDataOnce(0x0001, requestType, request, value, index, data);
  }
}
