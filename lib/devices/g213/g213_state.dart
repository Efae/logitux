part of 'g213_cubit.dart';

class G213State {
  double speed;
  String color;
  List<String> zones;

  G213State({required this.speed, required this.color, required this.zones});
}
