import 'package:libusb_new/libusb_new.dart';

import '../usb_device.dart';

class G910Driver extends UsbDevice {
  G910Driver()
      : super(vendorId: 0x046d, productId: 0xc335, displayName: 'Keyboard G910 Orion Spectrum');
}
