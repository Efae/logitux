import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AbstractPage extends StatefulWidget {
  final String title;
  final String baseRoute;
  final Map<IconData, Map<String, String>> menuItems;

  const AbstractPage(
      {super.key,
      required this.title,
      required this.baseRoute,
      required this.menuItems});

  @override
  State<AbstractPage> createState() => _AbstractPageState();
}

class _AbstractPageState extends State<AbstractPage> {
  late IconData currentIndex;

  @override
  void initState() {
    super.initState();
    currentIndex = widget.menuItems.keys.first;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Modular.to.navigate('/');
            },
          ),
        ),
        body: Row(
          children: [
            Flexible(
                flex: 0,
                child: ConstrainedBox(
                    constraints: const BoxConstraints(maxWidth: 300),
                    child: Container(
                        color: Colors.deepPurple,
                        child: Column(children: [
                          Center(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: widget.menuItems.keys
                                  .map((icon) => _menuCategory(icon))
                                  .toList(),
                            ),
                          ),
                          ConstrainedBox(
                            constraints: const BoxConstraints(maxHeight: 500),
                            child: NavigationListener(
                                builder: (context, child) => Drawer(
                                    backgroundColor: Colors.transparent,
                                    shape: const RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.all(Radius.zero)),
                                    child: ListView(
                                      children: widget
                                          .menuItems[currentIndex]!.entries
                                          .map((entry) =>
                                              _menuTile(entry.key, entry.value))
                                          .toList(),
                                    ))),
                          )
                        ])))),
            Expanded(
                flex: 1,
                child: Container(
                  color: Colors.deepPurple[100],
                  // color: Colors.red,
                  child: const RouterOutlet(),
                ))
          ],
        ));
  }

  Widget _menuCategory(IconData icon) {
    return Container(
        margin: const EdgeInsets.all(10),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
          child: Icon(icon),
          onPressed: () {
            Modular.to.navigate('${widget.baseRoute}/');
            setState(() => currentIndex = icon);
          },
        ));
  }

  Widget _menuTile(String title, String path) {
    return ListTile(
      selected: Modular.to.path.endsWith(path),
      selectedTileColor: Colors.deepPurple[100],
      textColor: Colors.white,
      title: Text(title),
      onTap: () {
        Modular.to.navigate('${widget.baseRoute}$path');
      },
    );
  }
}
