import 'package:flutter_modular/flutter_modular.dart';
import 'package:logitux/devices/blue_yeti/blue_yeti_driver.dart';
import 'package:logitux/devices/g213/g213.dart';
import 'package:logitux/devices/g502/g502_driver.dart';
import 'package:logitux/devices/g502/g502_module.dart';
import 'package:logitux/devices/g910/g910_driver.dart';
import 'package:logitux/devices/g915/g915_module.dart';
import 'package:logitux/devices/g915/g915_wired_driver.dart';
import 'package:logitux/devices/g915/g915_wireless_driver.dart';
import 'package:logitux/devices/generic_driver.dart';
import 'package:logitux/devices/usb_device.dart';

class DeviceModel {
  UsbDevice driver = GenericDriver(productId: 0, vendorId: 0);
  ModuleRoute? page;

  DeviceModel(vendorId, productId, name) {
    if (vendorId == 0x046d) {
      switch (productId) {
        case 0xc336:
          driver = G213Driver();
          page = ModuleRoute('/g213', module: G213Module(device: this));
          break;
        case 0xc335:
          driver = G910Driver();
          break;
        case 0xc08b:
          driver = G502Driver();
          page = ModuleRoute('/g502', module: G502Module(device: this));
          break;
        case 0x0ab7:
          driver = BlueYetiDriver();
          break;
        case 0xc33e:
          driver = G915WiredDriver();
          page = ModuleRoute('/g915', module: G915Module(device: this));
          break;
        case 0xc547:
          driver = G915WirelessDriver();
          page = ModuleRoute('/g915', module: G915Module(device: this));
        default:
          print('Unknown Logitech device: ${productId.toRadixString(16)}');
          driver = GenericDriver(productId: productId, displayName: name);
          break;
      }
    }
  }

  bool isLogitech() {
    return driver.vendorId == 0x046d;
  }

  @override
  String toString() {
    return 'DeviceModel{vendorId: ${driver.vendorId}, productId: ${driver.productId}, name: ${driver.name}}';
  }
}
