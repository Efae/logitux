import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:logitux/app_module.dart';
import 'package:logitux/device_model.dart';
import 'package:logitux/devices/g213/g213_cubit.dart';
import 'package:logitux/devices/usb_device.dart';

void main() {
  runApp(ModularApp(module: AppModule(), child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
          title: 'coucou',
          routerConfig: Modular.routerConfig,
        );
  }
}

class Home extends StatelessWidget {
  final List<DeviceModel> logiDevices =
      UsbDevice.getDeviceList().where((device) => device.isLogitech()).toList();

  Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Wrap(
                    children: logiDevices
                        .map((device) => GestureDetector(
                            onTap: () {
                              Modular.to.navigate(device.page!.name + '/');
                            },
                            child: Container(
                                width: 300,
                                height: 200,
                                margin: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white,
                                    boxShadow: const [
                                      BoxShadow(
                                          color: Colors.grey,
                                          offset: Offset(0, 3),
                                          blurRadius: 6)
                                    ]),
                                child: Center(
                                  child: Text(device.driver.name),
                                ))))
                        .toList()))));
  }
}
