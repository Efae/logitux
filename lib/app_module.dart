import 'package:flutter_modular/flutter_modular.dart';
import 'package:logitux/device_model.dart';
import 'package:logitux/devices/usb_device.dart';

import 'main.dart';

class AppModule extends Module {
  final List<DeviceModel> logiDevices =
      UsbDevice.getDeviceList().where((device) => device.isLogitech()).toList();

  @override
  void routes(r) {
    r.child('/', child: (context) => Home());
    logiDevices.where((device) => device.page != null).forEach((device) {
      r.add(device.page!);
    });
  }
}
